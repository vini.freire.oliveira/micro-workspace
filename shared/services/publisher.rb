# frozen_string_literal: true

module Services
  # Publisher para publicar na fila do RabbitMQ
  class Publisher
    class << self
      def call(message, queue, action)
        conn = Bunny.new(host: ENV['RABBITMQ_HOST'])
        conn.start

        channel = conn.create_channel
        queue = channel.queue("context-#{queue}")

        queue.publish(wrap(message, action))
        conn.stop
      end

      def wrap(message, action)
        {
          'correlation_id': message[:uid].to_s,
          'action': action,
          'content_type': 'text/plain',
          'message': message,
          'when': Time.now
        }.to_json
      end
    end
  end
end
