# frozen_string_literal: true

module Services
  class Consumer
    class << self
      def call(queue)
        conn = Bunny.new(host: ENV['RABBITMQ_HOST'])
        conn.start

        channel = conn.create_channel
        queue = channel.queue(queue)

        queue.subscribe do |delivery_info, properties, payload|
          message = JSON.parse(payload)["message"].symbolize_keys!

          case queue
          when 'context-user'
            return Services::CreateContextUser.call message
          when 'context-denunciation'
            return Services::CreateContextDenunciation.call message
          when 'context-emergency'
            return Services::CreateContextEmergency.call message
          when 'context-history'
            return Services::CreateContextHistory.call message
          else
            puts "queue not foud!"
          end
        end
        
        conn.stop
      end
    end
  end
end