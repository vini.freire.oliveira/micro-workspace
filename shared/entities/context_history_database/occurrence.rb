# frozen_string_literal: true

# Entity Occurrence
class Occurrence
  include Mongoid::Document
  include Mongoid::Timestamps

  store_in database: "context_history_database"

  field :full_name, type: String
  field :email, type: String
  field :cpf, type: String
  field :password, type: String
  field :nickname, type: String
  field :avatar, type: String

  validates_presence_of :name, :email, :cpf, :nickname
end
