params = {
  full_name: 'Vinicius Freire',
  email: 'vini@gmail.com',
  cpf: '617.301.143.34',
  password: '12345678',
  nickname: 'vini',
  avatar: 'url',
  address: {
    cep: '01141-000',
    street: 'Rua Dr. Rubens Meireles',
    street_number: 99,
    neighborhood: 'Varzea Barra Funda',
    longitude: '212312312',
    latitude: '154545445'
  },
  document: {
    kind: 'cpf',
    value: '617.301.143.34'
  }
}