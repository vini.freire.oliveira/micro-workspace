#!/bin/bash

set -ex

cd /database

if [ ! -e context_denunciation_database ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/gem_context_denunciation_database.git context_denunciation_database
fi

cd context_denunciation_database

unset BUNDLE_APP_CONFIG
bundle config set path '/gems'
bundle install

exec "$@"
