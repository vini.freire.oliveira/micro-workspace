#!/bin/bash

set -ex

cd /database

if [ ! -e context_user_database ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/gem_context_user_database.git context_user_database
fi

cd context_user_database

unset BUNDLE_APP_CONFIG
bundle config set path '/gems'
bundle install

exec "$@"
