#!/bin/bash

set -ex

cd /database

if [ ! -e context_history_database ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/gem_context_history_database.git context_history_database
fi

cd context_history_database

unset BUNDLE_APP_CONFIG
bundle config set path '/gems'
bundle install

exec "$@"
