#!/bin/bash

set -ex

cd /workspace

if [ ! -e micro-context-user ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/micro-context-user.git
fi

cd micro-context-user

unset BUNDLE_APP_CONFIG
bundle config set path '/gems'
bundle install

exec "$@"
