# RabbitMQ

## Credenciais de acesso

guest:guest

## Virtual Hosts

* cms_development
* cms_test
* cms_production
* videos_development
* videos_test
* videos_production

## Portas

Serviço não está mapeado no host (altere o docker-compose se necessário), no contêiner está na 5672

Interface de gerenciamento no host 15672 (no contêiner 55672)
