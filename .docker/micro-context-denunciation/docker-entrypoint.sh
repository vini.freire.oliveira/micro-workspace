#!/bin/bash

set -ex

cd /workspace

if [ ! -e micro-context-denunciation ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/micro-context-denunciation.git
fi

cd micro-context-denunciation

cd app/services
go build

cd ..

cd operations
go build

cd ..

rm go.mod
rm go.sum
go mod init denunciation/app
go install .

go run version.go app.go

exec "$@"
