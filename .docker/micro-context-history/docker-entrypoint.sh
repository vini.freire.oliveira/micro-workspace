#!/bin/sh

set -ex

cd /workspace

if [ ! -e micro-context-history ]; then
  git clone --recursive https://gitlab.com/vini.freire.oliveira/micro-context-history.git
fi

cd micro-context-history

exec "$@"
