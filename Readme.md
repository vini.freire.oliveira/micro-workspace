### Features

- Ecosystem for sending/receiving complaints in microservices;
- Api developed in ruby using Routing Tree Web Toolkit (RODA);
- Microservice developed in ruby to perform operations related to users;
- Microservice developed in go to perform operations related to denunciations;
- Microservice developed in python to perform operations related to history of denunciations and user;
- RabbitMQ as message brokers;
- Mongodb as database;
- Sidekiq to execute jobs in background
- Redis as key-value store in memory
- v2 API (RODA) REST request micro-services via gRPC and GraphQl


# System Architecture 

1.0 API with read access to databases over gem

![](./docs/micro-architecture-diagram.png)

- To use version 1.0 change workspace/micro-api/micro-context-denunciation/micro-context-history/micro-context-user to master branch

2.0 API REST request micro-services via gRPC and GraphQl 

![](./docs/micro-architecture-diagramv2.png)

- To use version 2.0 change workspace/micro-api/micro-context-denunciation/micro-context-history/micro-context-user to grpc branch

# API

1.0 API

![](./docs/api_v1.png)

2.0 API

![](./docs/api_v2.png)

# Micro Context User
- Microservice developed in ruby to perform operations related to users;
  - Users database operations create, update, find, delete
  - Connect with rabbitmq as consumer to perfom database operations
  - Implements grpc protocol to connect to api to given response as login, find user by token and others user operations

# Micro Context Denunciation
- Microservice developed in go to perform operations related to denunciations;
  - Denunciations database operations create, update, find, delete
  - Connect with rabbitmq as consumer to perfom database operations
  - Implements grpc protocol to connect to api to given response as all denunciations

# Micro Context History
- Microservice developed in python to perform operations related to history of denunciations and user;
  - Histories database operations create, update, find, delete
  - Connect with rabbitmq as consumer to perfom database operations
  - Implements graphQL protocol to connect to api to given response all histories

### Commands
docker network inspect workspace_default-micro